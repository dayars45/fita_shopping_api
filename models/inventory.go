package models

import (
	"database/sql"
)

type Inventory struct {
	ID    int     `json:"id"`
	Sku   string  `json:"sku"`
	Name  string  `json:"name"`
	Price float64 `json:"price"`
	Qty   int     `json:"qty"`
}

type InventoryCollection struct {
	Inventory []Inventory `json:"inventories"`
}

func GetInventory(db *sql.DB) InventoryCollection {
	sql := "SELECT * FROM inventory"
	rows, err := db.Query(sql)

	if err != nil {
		panic(err)
	}
	defer rows.Close()

	result := InventoryCollection{}
	for rows.Next() {
		inventory := Inventory{}
		err2 := rows.Scan(&inventory.ID, &inventory.Sku, &inventory.Name, &inventory.Price, &inventory.Qty)

		if err2 != nil {
			panic(err2)
		}
		result.Inventory = append(result.Inventory, inventory)
	}

	return result
}

func PutInventory(db *sql.DB, sku string, name string, price float64, qty int) (int64, error) {
	sql := "INSERT INTO inventory(sku, name, price, qty) VALUES(?,?,?,?)"

	stmt, err := db.Prepare(sql)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	result, err2 := stmt.Exec(sku, name, price, qty)
	if err2 != nil {
		panic(err2)
	}

	return result.LastInsertId()
}

func EditInventory(db *sql.DB, inventoryId int, sku string, name string, price float64, qty int) (int64, error) {
	sql := "UPDATE inventory set sku = ?, name = ?, price = ?, qty = ? WHERE id = ?"

	stmt, err := db.Prepare(sql)
	if err != nil {
		panic(err)
	}

	result, err2 := stmt.Exec(sku, name, price, qty, inventoryId)
	if err2 != nil {
		panic(err2)
	}

	return result.RowsAffected()
}

func DeleteInventory(db *sql.DB, id int) (int64, error) {
	sql := "DELETE FROM inventory WHERE id = ?"

	stmt, err := db.Prepare(sql)
	if err != nil {
		panic(err)
	}

	result, err2 := stmt.Exec(id)
	if err2 != nil {
		panic(err2)
	}

	return result.RowsAffected()
}
