package main

import (
	"context"
	"database/sql"
	"fita_shopping_api/handlers"
	"fmt"
	"time"

	"github.com/go-sql-driver/mysql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
)

func main() {
	e := echo.New()
	db := initDB("sicepat:sicepatmysql@tcp(localhost:3306)/db_fita_golang?parseTime=true") //"storage.db"
	migrate(db)

	e.GET("/inventory", handlers.GetInventory(db))
	e.POST("/inventory", handlers.PutInventory(db))
	e.PUT("/inventory", handlers.EditInventory(db))
	e.DELETE("/inventory/:id", handlers.DeleteInventory(db))

	e.Logger.Fatal(e.Start(":8000"))
}

func initDB(filepath string) *sql.DB {
	db, err := sql.Open("mysql", filepath) //sqlite3

	if err != nil {
		panic(err)
	}

	if db == nil {
		panic("db nil")
	}
	db.SetMaxIdleConns(10)                  //10 connection create
	db.SetMaxOpenConns(100)                 //max connection created
	db.SetConnMaxIdleTime(time.Minute * 5)  //delete connection in 5 minutes, any idle connection
	db.SetConnMaxLifetime(time.Minute * 60) //create new connection which is life in 60 minutes

	return db
}

func migrate(db *sql.DB) {
	sql := `
    CREATE TABLE IF NOT EXISTS inventory(
        id INTEGER(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
		SKU VARCHAR(10) NOT NULL UNIQUE,
		name VARCHAR(255) NOT NULL,
		price DECIMAL(10,2) NOT NULL,
		qty INTEGER(11) NOT NULL
    );
    `

	_, err := db.Exec(sql)
	if err != nil {
		panic(err)
	}

	mysql.RegisterLocalFile("data.csv")

	//DB Seeder
	_, err = db.ExecContext(context.Background(),
		`LOAD DATA LOCAL INFILE 'data.csv' INTO TABLE inventory
		FIELDS TERMINATED BY ','
		ENCLOSED BY '"'
		(sku, name, price, qty);
		`,
	)
	if err != nil {
		fmt.Println("db.ExecContext", err)
		// return
	}
	//End DB Seeder
}
