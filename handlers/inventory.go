package handlers

import (
	"database/sql"
	"fita_shopping_api/models"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

type H map[string]interface{}

func GetInventory(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.JSON(http.StatusOK, models.GetInventory(db))
	}
}

func PutInventory(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {

		var inventory models.Inventory

		c.Bind(&inventory)

		id, err := models.PutInventory(db, inventory.Sku, inventory.Name, inventory.Price, inventory.Qty)

		if err == nil {
			return c.JSON(http.StatusCreated, H{
				"created": id,
			})
		} else {
			return err
		}

	}
}

func EditInventory(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {

		var inventory models.Inventory
		c.Bind(&inventory)

		_, err := models.EditInventory(db, inventory.ID, inventory.Sku, inventory.Name, inventory.Price, inventory.Qty)

		if err == nil {
			return c.JSON(http.StatusOK, H{
				"updated": inventory,
			})
		} else {
			return err
		}
	}
}

func DeleteInventory(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id, _ := strconv.Atoi(c.Param("id"))

		_, err := models.DeleteInventory(db, id)

		if err == nil {
			return c.JSON(http.StatusOK, H{
				"deleted": id,
			})
		} else {
			return err
		}

	}
}
